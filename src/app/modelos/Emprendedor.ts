export interface Emprendedor {
// Primer Grupo
curp?: string;
nombre?: string;
apellido_paterno?: string;
apellido_materno?: string;
identificacion_oficial?: string;
foto_identificacion_adelante?: string;
foto_identificacion_atras?: string;
foto_personal?: string;
sexo?: string;
fecha_nacimiento?: Date;
numero_telefono?: number;
edad?: number;
correo?: string;
contrasena?: string;
// Segundo Grupo
estado?: string;
municipio?: string;
localidad?: string;
codigo_postal?: number;
tipo_vialidad?: string;
nombre_vialidad?: string;
numero_exterior?: number;
numero_interior?: number;
asentamiento?: string;
entre_vialidad_1?: string;
entre_vialidad_2?: string;
descripcion_ubicacion?: string;
// Tercer Grupo
etapa_proyecto?: string;
area_proyecto?: string;
estado_civil?: string;
jefe_familia?: string;
ocupacion?: string;
ingreso_mensual?: string;
numero_integrantes_familia?: number;
numero_habitantes_vivienda?: number;
numero_dependientes?: number;
nivel_estudios?: string;
escuela?: string;
tipo_seguridad_social?: string;
discapacidad?: string;
grupos_vulnerables?: string;
beneficiario_colectivo?: string;
como_enteraste_evento?: string;
tipo_vivienda?: string;
electricidad?: string;
agua?: string;
drenaje?: string;
telefono?: string;
internet?: string;

}
