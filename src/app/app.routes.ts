/* Rutas */
import { RouterModule, Routes } from '@angular/router';

/* Componentes */

import { NopagefoundComponent } from './shared/nopagefound/nopagefound.component';

import { LoginAdminComponent } from './pages/login-admin/login-admin.component';
import { PanelAdminComponent } from './pages/panel-admin/panel-admin.component';
import { AuthGuardService } from './services/authGuard/auth-guard.service';



/* Establecemos las rutas de nuestra aplicación */
const appRoutes: Routes = [


    { path: 'login', component: LoginAdminComponent},
    { path: 'administrador',  component: PanelAdminComponent},
    { path: 'datos', component: LoginAdminComponent},
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: '**', component: NopagefoundComponent }
];


export const APP_ROUTES = RouterModule.forRoot(appRoutes, {useHash: true} );
