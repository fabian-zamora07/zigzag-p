
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';


import { HttpClient } from '@angular/common/http';
import {URL_SERVICIOS} from '../../../config/urls';

import {ExcelService} from '../recursos/excel.service';


@Component({
  selector: 'app-panel-admin',
  templateUrl: './panel-admin.component.html',
  styleUrls: ['./panel-admin.component.scss']
})
export class PanelAdminComponent implements OnInit {

  emprendedor = this.obtenerParticipantes();
  

  constructor( 
               public http: HttpClient, private excelService: ExcelService
              ) {

  }

  ngOnInit() {
    
  }

 /* Regresa todos los regístros de la base de datos, convirtiendolos en un arreglo del objeto 'emprendedores'
    ya que esta en formato JSON y el ngFor solo acepta arrays pero no jala... */
    obtenerParticipantes() {
      this.http.get(URL_SERVICIOS + '/obtener_respuestas').subscribe(data => {
        var arreglo = data['Respuestas'];
        this.emprendedor = arreglo;
        console.log(data);
        
      },
      error => {
        console.log(error);
      }
      );
    }

  /* Cierra sesión del panel de administrador */
  logout() {
    
    
      console.log(event);
      location.href= '/#/login';
    

  }
 
exportAsXLSX() {this.http.get(URL_SERVICIOS + '/obtener_respuestas').subscribe(data => {
  var arreglo = data['Respuestas'];
  this.excelService.exportAsExcelFile(arreglo, 'Registro');
  console.log(data);
  
},
error => {
  console.log(error);
}
);
 
}

}
