/* Modulos */
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { ArchwizardModule } from 'angular-archwizard';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

/* Componentes */


import { LoginAdminComponent } from './login-admin/login-admin.component';
import { PanelAdminComponent } from './panel-admin/panel-admin.component';



@NgModule({
    declarations: [


        LoginAdminComponent,
        PanelAdminComponent
    ],

    exports: [

    ],

    imports: [
        SharedModule,
        RouterModule,
        ArchwizardModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule

    ]

})

export class PagesModule { }
